#
# Be sure to run `pod lib lint TNSocialNetWorkLogin.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'TNSocialNetWorkLogin'
  s.version          = '0.2.2'
  s.summary          = 'This library use for login by google and facebook.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
This library use for login by google and facebook. Make google login and facebook login to be easy.
                       DESC

  s.homepage         = 'https://gitlab.com/ntxuanthuy17/TNSocialNetworkLogin.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Thuỳ Nguyễn' => 'ntxuanthuy17@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/ntxuanthuy17/TNSocialNetworkLogin.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'
  s.swift_version = '4.0'
  s.source_files = 'TNSocialNetWorkLogin/Classes/**/*'
  s.static_framework = true
   s.resource_bundles = {
     'TNSocialNetWorkLogin' => ['TNSocialNetWorkLogin/Assets/*.png','TNSocialNetWorkLogin/Assets/*.bundle']
   }
  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.pod_target_xcconfig = { 'OTHER_LDFLAGS' => '-ObjC' }
  s.vendored_frameworks = 'TNSocialNetWorkLogin/Frameworks/*.framework'
  s.frameworks = 'UIKit', 'GoogleSignIn','GoogleSignInDependencies', 'FBSDKCoreKit', 'FBSDKLoginKit'
  #'SafariServices', 'SystemConfiguration', 'StoreKit', 'AddressBook'
  s.library = 'z'
  #s.dependency 'FBSDKCoreKit', '~> 4.0'
  #s.dependency 'FBSDKLoginKit', '~> 4.0'
end
