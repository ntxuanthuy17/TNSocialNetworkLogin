//
//  UIApplicationExtension.swift
//  Alamofire
//
//  Created by Thuỳ Nguyễn on 3/6/18.
//

import Foundation
import UIKit



public extension UIApplication {
    
    var topViewController: UIViewController? {

        guard keyWindow?.rootViewController != nil else {
            return keyWindow?.rootViewController
        }
        var topVC = keyWindow?.rootViewController
        while topVC?.presentedViewController != nil {
            switch topVC?.presentedViewController {
            case let navigationController as UINavigationController :
                topVC = navigationController.viewControllers.last
            case let tabBarController as UITabBarController :
                topVC = tabBarController.selectedViewController
            default:
                topVC = topVC?.presentedViewController
            }
        }
        return topVC
    }
}
