//
//  TNAuthenticationService.swift
//  Pods
//
//  Created by Thuỳ Nguyễn on 2/1/18.
//

import Foundation
import UIKit

public class TNAuthenticationService: TNAuthenticationable {

    private let googleService = TNGoogleService()
    private let facebookService = TNFacebookService()

    public func signInByFacebook(withReadPermissions: [Dictionary<String, [String]>]?, from vc: UIViewController?, completionHandler: @escaping ((LoginResult) -> Void)) {
        facebookService.signIn(withReadPermissions: withReadPermissions, from: vc, completionHandler: completionHandler
//            { (result) in
//            switch result {
//            case .success(let user):
//                TNAuthenticationManager.shared.setCurrentUser(from: user)
//                completionHandler(result)
//                break
//            default:
//                completionHandler(result)
//                break
//            }
//        }
        )
    }

    public func signInByGoogle(completionHandler: @escaping ((LoginResult) -> Void)) {
        googleService.signInByGoogle(completionHandler:completionHandler
//            { (result) in
//            switch result {
//            case .success(let user):
//                TNAuthenticationManager.shared.setCurrentUser(from: user)
//                completionHandler(result)
//                break
//            default:
//                completionHandler(result)
//                break
//            }
//        }
        )
    }

    public func signOut() {
        facebookService.signOut()
        googleService.signOut()
//        TNAuthenticationManager.shared.setCurrentUser(from: nil)
    }

    public func checkAuthentication() -> Bool {
        return facebookService.checkAuthentication() || googleService.checkAuthentication() ? true : false
    }
}
