//
//  TNAuthenticationManager.swift
//  Pods
//
//  Created by Thuỳ Nguyễn on 2/1/18.
//

import Foundation
import UIKit

public class TNAuthenticationManager {

    public static let shared = TNAuthenticationManager()
//    private var _currentUser: TNUser?
    private lazy var authentication: TNAuthenticationable = TNAuthenticationService()

    private init() {
//        if let user = UserDefaults.standard.object(forKey: "TNUser") as? Dictionary<String, Any?> {
//            _currentUser = TNUser(from: user)
//        }

    }

//    public func currentUser() ->TNUser? {
//        return _currentUser
//    }

//    func setCurrentUser(from user: TNUser?) {
//        _currentUser = user
//        UserDefaults.standard.set(user?.dictionaryValue(), forKey: "TNUser")
//    }

    public func obtainAuthentication() -> TNAuthenticationable {
        return authentication
    }

    public func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        TNFacebookService.application(application, didFinishLaunchingWithOptions: launchOptions)

    }
}

