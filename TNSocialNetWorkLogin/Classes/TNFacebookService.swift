//
//  TNFacebookService.swift
//  Pods
//
//  Created by Thuỳ Nguyễn on 2/1/18.
//

import Foundation
import FBSDKCoreKit
import FBSDKLoginKit

class TNFacebookService {
    private enum FacebookKey: String {
        case userId = "id"
        case email = "email"
        case name = "name"
        case lastName = "last_name"
        case firstName = "first_name"
        case picture = "picture"
    }

    private let login = FBSDKLoginManager()


//    func signInByFacebook(from vc: UIViewController, completionHandler: @escaping ((LoginResult) -> Void)) {
//        login.logIn(withReadPermissions: ["public_profile","email"], from: nil) { (result, error) in
//            if error != nil {
//                return completionHandler(.error(error: error!))
//            }
//            if let result = result {
//                if result.isCancelled {
//                    return completionHandler(.cancel)
//                }
//                TNFacebookService.getInfomation(accessToken: result.token.tokenString, completionHandler: { (user) in
//                    return completionHandler(.success(user: user))
//                }, errorHandler: { (getInfoError) in
//                    return completionHandler(.error(error: getInfoError))
//                })
//            }
//        }
//    }

    func signOut(){
        login.logOut()
    }

    func checkAuthentication() -> Bool {
        return FBSDKAccessToken.current() != nil
    }

    static func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        // Override point for customization after application launch.
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }

//    static func getInfomation(accessToken: String, completionHandler: ((TNUser) -> Void)?, errorHandler: ((Error) -> Void)?) {
//        let fields = [FacebookKey.email.rawValue,FacebookKey.name.rawValue, FacebookKey.lastName.rawValue, FacebookKey.firstName.rawValue, "picture.width(480).height(480)"]
//        let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": fields.joined(separator: ",")], tokenString: accessToken, version: nil, httpMethod: "GET")
//        _ = req?.start(completionHandler: { (connection, result, error) in
//            if let r = result as? Dictionary<String, Any> {
//                if let userId = r[FacebookKey.userId.rawValue] as? String {
//                    let user = TNUser(userId: userId, accessToken: accessToken)
//                    user.name = r[FacebookKey.name.rawValue] as? String
//                    user.lastName = r[FacebookKey.lastName.rawValue] as? String
//                    user.firstName = r[FacebookKey.firstName.rawValue] as? String
//                    user.email = r[FacebookKey.email.rawValue] as? String
//                    if let picture = r[FacebookKey.picture.rawValue] as? Dictionary<String, Any>{
//                        if let data = picture["data"] as? Dictionary<String, Any> {
//                            if let url = data["url"] as? String {
//                                user.picturePath = url
//                            }
//                        }
//                    }
//                    completionHandler?(user)
//                    return
//                }
//                let getInfoError = NSError(domain: "Can't get user info from facebook", code: 404, userInfo: nil)
//                errorHandler?(getInfoError)
//                return
//            }
//            if let error = error {
//                errorHandler?(error)
//                return
//            }
//        })
//    }


    func signIn(withReadPermissions permissions: [Dictionary<String, [String]>]?, from vc: UIViewController?, completionHandler: @escaping ((LoginResult) -> Void)) {
        var permissions = permissions
        var params:[String] = []
        if permissions == nil {
            let permissionObject = TNFacebookPermission()
            permissions = [permissionObject.email, permissionObject.publicProfile]
            params = [FacebookKey.email.rawValue,FacebookKey.name.rawValue, FacebookKey.lastName.rawValue, FacebookKey.firstName.rawValue, "picture.width(480).height(480)"]
        }else {
            permissions!.forEach { (dic) in
                params += dic.values.first ?? []
            }
        }
        let readPermissions = permissions!.map({$0.keys.first ?? "" })

        login.logIn(withReadPermissions: readPermissions, from: vc) { (result, error) in
            if error != nil {
                return completionHandler(.error(error: error!))
            }
            if let result = result {
                if result.isCancelled {
                    return completionHandler(.cancel)
                }
                TNFacebookService.getInfomation(parameters: params, accessToken: result.token.tokenString, completionHandler: { (user) in
                    return completionHandler(.success(user: user))
                }, errorHandler: { (getInfoError) in
                    return completionHandler(.error(error: getInfoError))
                })
            }
        }

    }

    static func getInfomation(parameters: [String], accessToken: String, completionHandler: ((TNUser) -> Void)?, errorHandler: ((Error) -> Void)?) {
        let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": parameters.joined(separator: ",")], tokenString: accessToken, version: nil, httpMethod: "GET")
        _ = req?.start(completionHandler: { (connection, result, error) in
            if let error = error {
                errorHandler?(error)
                return
            }
            if let r = result as? Dictionary<String, Any> {
                let user = TNUser(accessToken: accessToken)
                user.userId = r[FacebookKey.userId.rawValue] as? String
                user.name = r[FacebookKey.name.rawValue] as? String
                user.lastName = r[FacebookKey.lastName.rawValue] as? String
                user.firstName = r[FacebookKey.firstName.rawValue] as? String
                user.email = r[FacebookKey.email.rawValue] as? String
                if let picture = r[FacebookKey.picture.rawValue] as? Dictionary<String, Any>{
                    if let data = picture["data"] as? Dictionary<String, Any> {
                        if let url = data["url"] as? String {
                            user.picturePath = url
                        }
                    }
                }
                user.rawValues = r
                completionHandler?(user)
                return
            }else {
                let getInfoError = NSError(domain: "Can't get user info from facebook", code: 404, userInfo: nil)
                errorHandler?(getInfoError)
                return
            }
        })
    }

}
