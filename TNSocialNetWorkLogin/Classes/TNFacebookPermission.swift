//
//  TNFacebookPermission.swift
//  Pods-TNSocialNetWorkLogin_Example
//
//  Created by Thuỳ Nguyễn on 2/2/18.
//

import Foundation

public class TNFacebookPermission: NSObject {
    public var email: Dictionary<String, [String]> =  ["email" : ["email"]]
    public var publicProfile: Dictionary<String, [String]> =  ["public_profile" : ["id", "cover", "name", "first_name", "last_name", "age_range", "link", "gender", "locale", "picture", "timezone", "updated_time", "verified"]]
    public var userFriends: Dictionary<String, [String]> =  ["user_friends" : ["uid"]]
    public var aboutMe: Dictionary<String, [String]> =  ["user_about_me" : ["about"]]
    public var birthday: Dictionary<String, [String]> =  ["user_birthday" : ["birthday"]]
    public var educationHistory: Dictionary<String, [String]> =  ["user_education_history" : ["education"]]
    public var hometown: Dictionary<String, [String]> =  ["user_hometown" : ["hometown"]]
    public var location: Dictionary<String, [String]> =  ["user_location" : ["location"]]
    public var relationshipDetails: Dictionary<String, [String]> =  ["user_relationship_details" : ["interested_in"]]
    public var relationships: Dictionary<String, [String]> =  ["user_relationships" : ["relationship_status", "significant_other", "family"]]
    public var religionPolitic: Dictionary<String, [String]> =  ["user_religion_politics" : ["religion", "political"]]
    public var workHistory: Dictionary<String, [String]> =  ["user_work_history" : ["work"]]

    public override init() {
        super.init()
    }
}
