//
//  TNUser.swift
//  Pods
//
//  Created by Thuỳ Nguyễn on 2/1/18.
//

import Foundation
fileprivate enum TNUserKey: String {
    case email = "email"
    case name = "name"
    case lastName = "lastName"
    case firstName = "firstName"
    case picturePath = "picturePath"
    case userId = "userId"
    case accessToken = "accessToken"
    case idToken = "idToken"
    case rawValues = "rawValues"
}

public class TNUser: NSObject {

    public var email: String?
    public var name: String?
    public var lastName: String?
    public var firstName: String?
    public var picturePath: String?
    public var userId: String?
    public var accessToken: String
    public var rawValues: Dictionary<String, Any>?

    init(accessToken: String) {
        self.accessToken = accessToken
        super.init()
    }

    init(from dic: Dictionary<String, Any?>){
        accessToken = dic[TNUserKey.accessToken.rawValue] as! String
        email = dic[TNUserKey.email.rawValue] as? String
        name = dic[TNUserKey.name.rawValue] as? String
        lastName = dic[TNUserKey.lastName.rawValue] as? String
        firstName = dic[TNUserKey.firstName.rawValue] as? String
        picturePath = dic[TNUserKey.picturePath.rawValue] as? String
        userId = dic[TNUserKey.userId.rawValue] as? String
        rawValues = dic[TNUserKey.rawValues.rawValue] as? Dictionary<String, Any>
    }

    func dictionaryValue() -> Dictionary<String, Any> {
        return [TNUserKey.email.rawValue: email ?? "",
                TNUserKey.name.rawValue: name ?? "",
                TNUserKey.lastName.rawValue: lastName ?? "",
                TNUserKey.firstName.rawValue: firstName ?? "",
                TNUserKey.picturePath.rawValue: picturePath ?? "",
                TNUserKey.userId.rawValue: userId ?? "",
                TNUserKey.accessToken.rawValue: accessToken,
                TNUserKey.rawValues.rawValue: rawValues ?? []
        ]
    }
}

public class TNGoogleUser: TNUser {

    public var idToken: String

    init(userId: String, accessToken: String, idToken: String) {
        self.idToken = idToken
        super.init(accessToken: accessToken)
    }

    override init(from dic: Dictionary<String, Any?>) {
        idToken = dic[TNUserKey.idToken.rawValue] as! String
        super.init(from: dic)
    }

    override func dictionaryValue() -> Dictionary<String, Any> {
        var dic = super.dictionaryValue()
        dic[TNUserKey.idToken.rawValue] = idToken
        return dic
    }
}

public enum LoginResult {
    case success(user: TNUser)
    case error(error: Error)
    case cancel
}
