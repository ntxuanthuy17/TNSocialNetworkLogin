//
//  TNAuthenticationable.swift
//  Pods
//
//  Created by Thuỳ Nguyễn on 2/1/18.
//

import Foundation
import UIKit

public protocol TNAuthenticationable {
    func signInByFacebook(withReadPermissions: [Dictionary<String, [String]>]?, from vc: UIViewController?, completionHandler: @escaping ((LoginResult) -> Void))

//    func signInByFacebook(from vc: UIViewController, completionHandler: @escaping ((LoginResult) -> Void))
    func signInByGoogle(completionHandler: @escaping ((LoginResult) -> Void))
    func signOut()
    func checkAuthentication()-> Bool 
    
}
