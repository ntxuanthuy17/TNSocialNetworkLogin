//
//  TNGoogleService.swift
//  Pods-TNSocialNetWorkLogin_Example
//
//  Created by Thuỳ Nguyễn on 2/1/18.
//

import Foundation
import GoogleSignIn

class TNGoogleService: NSObject, GIDSignInDelegate, GIDSignInUIDelegate {

    private var loginCompletionHandler: ((LoginResult) -> Void)?

    override init() {
        super.init()
        if let kClientID = Bundle.main.object(forInfoDictionaryKey: "GoogleClientID") as? String {
            GIDSignIn.sharedInstance().clientID = kClientID
            GIDSignIn.sharedInstance().uiDelegate = self
            GIDSignIn.sharedInstance().delegate = self
//            GIDSignIn.sharedInstance().signIn()
        }else {
//            fatalError("Error: Please add google ClientID into GoogleClientID key in your info.plist file. You can find your ClientID at GoogleService-Info.plist")
        }
    }

    func signInByGoogle(completionHandler: @escaping ((LoginResult) -> Void)) {
        self.loginCompletionHandler = completionHandler
//        if let kClientID = Bundle.main.object(forInfoDictionaryKey: "GoogleClientID") as? String {
//            GIDSignIn.sharedInstance().clientID = kClientID
            GIDSignIn.sharedInstance().uiDelegate = self
            GIDSignIn.sharedInstance().delegate = self
        if GIDSignIn.sharedInstance().clientID != nil {
            GIDSignIn.sharedInstance().signIn()
        }else {
            fatalError("Error: Please add google ClientID into GoogleClientID key in your info.plist file. You can find your ClientID at GoogleService-Info.plist")
        }
    }

    func signOut() {
        GIDSignIn.sharedInstance().signOut()
    }

    func checkAuthentication() -> Bool {
        return GIDSignIn.sharedInstance().hasAuthInKeychain()
    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            let err = error as NSError
            if err.code == GIDSignInErrorCode.canceled.rawValue {
                //User canceled
                loginCompletionHandler?(.cancel)
            }else {
                //another error
                loginCompletionHandler?(.error(error: error))
            }
            loginCompletionHandler = nil
            return
        }
        let tnUser = TNGoogleUser(userId: user.userID, accessToken: user.authentication.accessToken, idToken: user.authentication.idToken)
        tnUser.name = user.profile.name
        tnUser.email = user.profile.email
        tnUser.firstName = user.profile.givenName
        tnUser.lastName = user.profile.familyName
        if user.profile.hasImage {
            tnUser.picturePath = user.profile.imageURL(withDimension: 480).absoluteString
        }
        loginCompletionHandler?(.success(user: tnUser))
        loginCompletionHandler = nil
        return
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {

    }

    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
         viewController.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        UIApplication.shared.topViewController?.present(viewController, animated: true, completion: nil)
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {

    }
}
