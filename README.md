# TNSocialNetWorkLogin

[![Version](https://img.shields.io/cocoapods/v/TNSocialNetWorkLogin.svg?style=flat)](http://cocoapods.org/pods/TNSocialNetWorkLogin)
[![License](https://img.shields.io/cocoapods/l/TNSocialNetWorkLogin.svg?style=flat)](http://cocoapods.org/pods/TNSocialNetWorkLogin)
[![Platform](https://img.shields.io/cocoapods/p/TNSocialNetWorkLogin.svg?style=flat)](http://cocoapods.org/pods/TNSocialNetWorkLogin)

This library provides a simple way to login by social network.

## Requirements

- iOS 9.0+
- Xcode 9.2+
- Swift 4.0+

## Installation

TNSocialNetWorkLogin is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TNSocialNetWorkLogin'
```

## How to use

#### :point_right:  Facebook Login
**Step 1: Configure Facebook App Settings for iOS**

As the normal way, you have to configure your Facebook App. For detail you can see on [Facebook document](https://developers.facebook.com/docs/ios/getting-started#settings).

**Step 2: Configure Info.plist**

1. In Xcode, right-click your project's **Info.plist** file and select Open As -> Source Code.

2. Insert the following XML snippet into the body of your file just before the final ```</dict>``` element.

```xml
<key>CFBundleURLTypes</key>
<array>
  <dict>
    <key>CFBundleURLSchemes</key>
    <array>
      <string>fb{your-app-id}</string>
    </array>
  </dict>
</array>
<key>FacebookAppID</key>
<string>{your-app-id}</string>
<key>FacebookDisplayName</key>
<string>{your-app-name}</string>
<key>LSApplicationQueriesSchemes</key>
<array>
  <string>fbapi</string>
  <string>fb-messenger-share-api</string>
  <string>fbauth2</string>
  <string>fbshareextension</string>
</array>
```

3. Replace **{your-app-id}**, and **{your-app-name}** with your app's App's ID and name found on the Facebook App Dashboard.

**Step 3: Config at your AppDelegate**

1. In your app delegate(`AppDelegate.swift`), import library
```swift
import TNSocialNetWorkLogin
...
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
```
2. In your app delegate's `application:didFinishLaunchingWithOptions:` method, configure the `TNSocialNetWorkLogin` shared instance.
```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

    TNAuthenticationManager.shared.application(application, didFinishLaunchingWithOptions: launchOptions)

return true
}
```

**Step 4: Add Facebook Login to Your Code**

1. Import the
For basic login, you can pass nil for permissions. It will try to get user id, email, last name, first name and picture of user.

```swift
func signInByFacebook() {
    TNAuthenticationManager.shared.obtainAuthentication().signInByFacebook(withReadPermissions: nil, from: nil) { (result) in
        switch result {
            case .error(let error):
                //Got an error while login.
                print(error.localizedDescription)
                break
            case .cancel:
                //When user canceled.
                print("the user cancel to login")
                break
            case .success(let user):
                //Login success
                print(user.name)
                break
        }
    }
}
```


For advanced login, you can setting a **TNFacebookPermission** object to get more information. By default, the TNFacebookPermission object defined permissions and paramss follow [Facebook permission v2.4](https://developers.facebook.com/docs/facebook-login/permissions/v2.4#user-data).
So you can use some available permissions as `permission.birthday` at the example. Or you can change these permissions as `permission.publicProfile` at the example:

```swift
let permission = TNFacebookPermission()
permission.publicProfile = ["public_profile": ["name"]]
TNAuthenticationManager.shared.obtainAuthentication().signInByFacebook(withReadPermissions: [permission.publicProfile, permission.birthday], from: nil) { (result) in
    switch result {
	    case .error(let error):
	        //Got an error while login.
	        print(error.localizedDescription)
	        break
	    case .cancel:
	        //When user canceled.
	        print("the user cancel to login")
	        break
	    case .success(let user):
	        //Login success
	        print(user.name)
	        print(user.rawValues)
	        break
    }
}
```

This library just supports some normal fields in **TNUser**. If you want to get more informations, you can get in the `rawValues` property. This value is the same with result of Facebook response.


#### :point_right:  Google Login
**Step 1: Configure a Google API console project**

Click on [Configure a project](https://developers.google.com/mobile/add?platform=ios&cntapi=signin&cntapp=Default+Demo+App&cntpkg=com.google.samples.quickstart.SignInExample&cnturl=https%3A%2F%2Fdevelopers.google.com%2Fidentity%2Fsign-in%2Fios%2Fstart%3Fver%3Dswift%26configured%3Dtrue&cntlbl=Continue+with+Try+Sign-In&hl=vi) to configure a Google API console project. You must select an existing project for your app or create a new one. You'll also need to provide a bundle ID for your app. Then download file `GoogleService-Info.plist`.

**Step 2: Configure Info.plist**

1. In Xcode, right-click your project's **Info.plist** file and select Open As -> Source Code.

2. Insert the following XML snippet into the body of your file just before the final ```</dict>``` element.
```xml
<key>GoogleClientID</key>
<string>{your-client-id}</string>
```

3. Replace **{your-client-id}** with your `CLIENT_ID` and found on `CLIENT_ID` field in your **GoogleService-Info.plist file**.

**Step 3: Configure URL Schemes**

In your project's **Info** tab, under the **URL Types** section, find the **URL Schemes** box containing the string `YOUR_REVERSED_CLIENT_ID`. Replace this string with the value of the `REVERSED_CLIENT_ID` string in **GoogleService-Info.plist**.
![Add REVERSED_CLIENT_ID](Images/add_reversed_client_id.png)

**Step 4: Add Google Login to Your Code**
```swift
func signInByGoogle() {
    TNAuthenticationManager.shared.obtainAuthentication().signInByGoogle() { (result) in
        switch result {
            case .error(let error):
                //Got an error while login.
                print(error.localizedDescription)
                break
            case .cancel:
                //When user canceled.
                print("the user cancel to login")
                break
            case .success(let user):
                //Login success
                print(user.name)
                break
        }
    }
}
```

## Author

Thuỳ Nguyễn, ntxuanthuy17@gmail.com

## License

TNSocialNetWorkLogin is available under the MIT license. See the LICENSE file for more info.
